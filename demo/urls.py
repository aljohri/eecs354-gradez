from django.conf.urls import patterns, include, url
from django.conf.urls.defaults import *
from django.contrib import admin
admin.autodiscover()

# urlpatterns = patterns('',
#     url(r'^admin/', include(admin.site.urls)),	
#     url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
#     url(r'^gradez/', include('demo.apps.gradez.urls')),
#     url(r'^login/$', 'demo.apps.auth.views.login_user'),    
#   	url(r'^$', 'demo.views.main_page'),
#     url(r'^googlefb3c0f5dc52a8a4c', 'django.views.generic.simple.direct_to_template', {'template': 'googlefb3c0f5dc52a8a4c.html'}),  	
# )



urlpatterns = patterns('',
    (r'^$', 'demo.views.main_page'),

    # Admin
    url(r'^admin/', include(admin.site.urls)),	
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Login / logout.
    #(r'^login/$', 'demo.apps.auth.views.login_user'),    
    (r'^login/$', 'django.contrib.auth.views.login'),
    (r'^logout/$', 'demo.views.logout_page'),

    # Gradez Application.
    (r'^gradez/', include('demo.apps.gradez.urls')),

    # Serve static content.
    #(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': 'static'}),

    # accounts.google.com/managedomains if heroku app changes
    (r'^googlefb3c0f5dc52a8a4c', 'django.views.generic.simple.direct_to_template', {'template': 'googlefb3c0f5dc52a8a4c.html'}),
)

