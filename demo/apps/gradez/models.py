from django.db import models

class OAuthToken(models.Model):
    token = models.CharField(max_length=255)
    token_secret = models.CharField(max_length=255)
    auth_state = models.IntegerField()
    next = models.CharField(max_length=255)
    verifier = models.CharField(max_length=255)

    def __unicode__(self):
         return self.token

 #return u'token: %s, secret: %s, active: %s' % (self.token) (self.token_secret) (self.active)

# REQUEST_TOKEN = 1
# AUTHORIZED_REQUEST_TOKEN = 2
# ACCESS_TOKEN = 3


# request_token.token = token
# request_token.token_secret = token_secret
# request_token.auth_state = ACCESS_TOKEN
# request_token.next = None
# request_token.verifier = None


# from demo.apps.gradez.models import OAuthToken