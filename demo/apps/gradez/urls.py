from django.conf.urls.defaults import *

urlpatterns = patterns('',
  (r'^get_oauth_token', 'demo.apps.gradez.views.get_oauth_token'),
  (r'^get_access_token', 'demo.apps.gradez.views.get_access_token'),
  (r'^import_spreadsheet', 'demo.apps.gradez.views.import_spreadsheet'),
  (r'^$', 'demo.apps.gradez.views.main_page'),
)
