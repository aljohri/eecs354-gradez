# http://deathofagremmie.com/2011/07/04/implementing-oauth-using-google-s-python-client-library/
# http://blog.pamelafox.org/2010/09/using-oauth-with-spreadsheets-api-on.html
#import pdb; pdb.set_trace()

from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib.auth import logout
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required

import cgi
import os
import logging
import json

import gdata.auth
import gdata.spreadsheets.client
import gdata.spreadsheets.data

from demo.apps.gradez.models import OAuthToken

#from pudb import set_trace;

CONSUMER_KEY = 'still-stream-7744.herokuapp.com' #'stark-dawn-1008.herokuapp.com' #'anonymous'
CONSUMER_SECRET = 'S9bQdQ8Kj9yaZSQoC4c3s0Tg' #'UaBXIXSZwkVLaX7mEu1Or2Rf' #'anonymous'
SCOPES = ['https://spreadsheets.google.com/feeds/']

client = gdata.spreadsheets.client.SpreadsheetsClient()

@login_required
def main_page(request):
  #set_trace()

  if (not OAuthToken.objects.all()):
    return HttpResponseRedirect('/gradez/get_oauth_token')
  else:
    return HttpResponseRedirect('/gradez/import_spreadsheet/')

def get_oauth_token(request):
  #set_trace()

  if(request.get_host()=='still-stream-7744.herokuapp.com'):
    oauth_callback_url = 'http://%s/gradez/get_access_token' % (request.get_host())  
    #oauth_callback_url = 'http://%s:%s/gradez/get_access_token' % (request.META.get('SERVER_NAME'), request.META.get('SERVER_PORT'))
  else:
    oauth_callback_url = 'http://%s:%s/gradez/get_access_token' % ('localhost', request.META.get('SERVER_PORT'))
  
  request_token = client.GetOAuthToken(SCOPES, oauth_callback_url, CONSUMER_KEY, consumer_secret=CONSUMER_SECRET)

  #request.session['REQ_TOKEN_SESSION_KEY'] = request_token # save in DB!!
  token = OAuthToken(token=request_token.token, token_secret=request_token.token_secret, auth_state=request_token.auth_state, next="", verifier="")
  token.save()

  authorization_url = str(request_token.generate_authorization_url())
  return HttpResponseRedirect(authorization_url)


"""
https://accounts.google.com/OAuthAuthorizeToken?oauth_token=4%2FQweJVF6F63DvRq2JceiwI5iQIY74&hd=default
"""

def get_access_token(request):
  #set_trace()
  #saved_request_token = request.session['REQ_TOKEN_SESSION_KEY']
  saved_request_token = OAuthToken.objects.all()[0]
  saved_request_token = gdata.gauth.OAuthHmacToken(CONSUMER_KEY.encode('ascii'), CONSUMER_SECRET.encode('ascii'), saved_request_token.token.encode('ascii'), saved_request_token.token_secret.encode('ascii'), saved_request_token.auth_state)
  request_token = gdata.gauth.AuthorizeRequestToken(saved_request_token, request.build_absolute_uri())
  access_token = client.GetAccessToken(request_token)

  saved_request_token = OAuthToken.objects.all()[0]
  #request.session['REQ_TOKEN_SESSION_KEY'] = access_token
  saved_request_token.token = access_token.token
  saved_request_token.token_secret = access_token.token_secret
  saved_request_token.auth_state = access_token.auth_state
  saved_request_token.next = ""
  saved_request_token.verifier = ""
  saved_request_token.save()

  return HttpResponseRedirect('/gradez/')

def import_spreadsheet(request):
  #import re
  #import models

  import atom.http_core

  #TOKEN = request.session['REQ_TOKEN_SESSION_KEY'].token
  #TOKEN_SECRET = request.session['REQ_TOKEN_SESSION_KEY'].token_secret
  saved_request_token = OAuthToken.objects.all()[0]
  TOKEN = saved_request_token.token.encode('ascii')
  TOKEN_SECRET = saved_request_token.token_secret.encode('ascii')
  client.auth_token = gdata.gauth.OAuthHmacToken(CONSUMER_KEY, CONSUMER_SECRET, TOKEN, TOKEN_SECRET, gdata.gauth.ACCESS_TOKEN)

  spreadsheet_key = '0AqfP9C6R7wKrdFk1OXdtc2Nnck5EbU1NNFRXcmZFb2c' # '0AqfP9C6R7wKrdGRYOGc1V000ZzJpdVRWLWN1dEl4ekE' #gradez db
  worksheet_id = 'od6' #default worksheet
  list_feed = 'https://spreadsheets.google.com/feeds/list/%s/%s/private/values' % (spreadsheet_key, worksheet_id)
  feed = client.get_feed(list_feed, desired_class=gdata.spreadsheets.data.ListsFeed)

  from collections import OrderedDict
  for row in feed.entry:
    if(row.get_value('netid') == request.user.username):
      #set_trace()
      student = OrderedDict([('netid' , row.get_value('netid')), ('fullname' , row.get_value('fullname')), ('email' , row.get_value('email')), ('lab1' , row.get_value('lab1')), ('lab2' , row.get_value('lab2')), ('lab3' , row.get_value('lab3')), ('lab4' , row.get_value('lab4')), ('lab5' , row.get_value('lab5')), ('lab6' , row.get_value('lab6')), ('proj1' , row.get_value('proj1')), ('proj2' , row.get_value('proj2')), ('proj3' , row.get_value('proj3')), ('proj4' , row.get_value('proj4'))])
      break

  response = json.dumps(student, indent=2, separators=(',',': '))
  return HttpResponse(response, mimetype="application/json")

